package com.ssa.smslarav.fragments


import android.annotation.SuppressLint
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import com.ssa.smslarav.R
import kotterknife.bindView


class MainFragment : BaseFragment() {

    //    var mWebView: WebView by bindView(R.id.webView)
    val logo: View by bindView(R.id.logo)


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    companion object {
        fun newInstance(param1: String, param2: String): MainFragment {
            val fragment = MainFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mWebView = view.findViewById<WebView>(R.id.webView)
        mWebView?.isVerticalScrollBarEnabled = false
        mWebView?.isHorizontalScrollBarEnabled = false
        mWebView?.settings?.javaScriptEnabled = true
        mWebView?.settings?.allowFileAccessFromFileURLs = true //Maybe you don't need this rule
        mWebView?.settings?.allowUniversalAccessFromFileURLs = true
        mWebView?.settings?.allowFileAccess = true
        mWebView?.settings?.setAppCacheEnabled(true)
        mWebView?.webChromeClient = WebChromeClient()
        mWebView?.webViewClient = WebViewClient()

        mWebView?.settings?.setAppCacheMaxSize(5 * 1024 * 1024); // 5MB
        mWebView?.settings?.setAppCachePath(context?.applicationContext?.cacheDir?.absolutePath)


        mWebView?.settings?.cacheMode = WebSettings.LOAD_DEFAULT // load online by default

        if (!isNetworkAvailable()) { // loading offline
            mWebView?.settings?.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
        }


        mWebView?.loadUrl("https://smslarav.co.il/")

        logo.visibility = View.GONE
    }

    class MyWebViewClient(val progressView: View) : WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                view?.loadUrl(request?.url.toString())
            }
            return true
        }

        override fun onLoadResource(view: WebView?, url: String?) {
            if (url != null) {
                if (url.startsWith("sms")) {

                } else if (url.startsWith("tel")) {

                }
            }
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            progressView.visibility = View.GONE
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = context?.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}

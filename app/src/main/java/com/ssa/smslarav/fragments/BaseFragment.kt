package com.ssa.smslarav.fragments

import android.content.Context
import android.support.v4.app.Fragment

open class BaseFragment : Fragment() {

    private var mListener: BaseFragmentListener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is BaseFragmentListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement BaseFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface BaseFragmentListener {
        fun add(fragment: BaseFragment, addToBackStack: Boolean)
        fun replace(fragment: BaseFragment, addToBackStack: Boolean)
    }

}
